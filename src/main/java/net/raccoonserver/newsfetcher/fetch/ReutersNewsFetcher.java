package net.raccoonserver.newsfetcher.fetch;

import net.raccoonserver.newsfetcher.utils.JsonReader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReutersNewsFetcher {
    public static String generateRSS() throws IOException, JSONException {
        StringBuilder str = new StringBuilder();

        str.append("<?xml version='1.0' encoding='UTF-8'?>\n");
        str.append("<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:dc='http://purl.org/dc/elements/1.1/' >\n");
        str.append("<channel>\n");
        str.append("<title>Reuters</title>\n");
        str.append("<language>en-us</language>\n");
        str.append("<link>https://www.reuters.com/theWire</link>\n");
        str.append("<description>Top Headline's from Reuters.</description>\n");

        for(JSONObject story : getStoryObjects(getWireURLs())){
            str.append("<item>\n");
            str.append("<title><![CDATA[" + getStoryHeadline(story) + "]]></title>\n");
            str.append("<link>" + getStoryShareURL(story) + "</link>\n");

            str.append("<description><![CDATA[<p>\n");
            str.append(getStoryBody(story));
            str.append("\n]]></description>\n");

            str.append("<pubDate>" + getStoryPublished(story) + "</pubDate>\n");
            str.append("</item>\n");
        }

        str.append("</channel></rss>");
        return str.toString();
    }

    private static List<JSONObject> getStoryObjects(List<String> storyURLs) throws IOException, JSONException {
        List<JSONObject> storyObjects = new ArrayList<JSONObject>();

        for(String storyURL : storyURLs){
            storyObjects.add(JsonReader
                    .readJsonFromUrl(storyURL)
                    .getJSONArray("wireitems")
                    .getJSONObject(0)
                    .getJSONArray("templates")
                    .getJSONObject(0)
                    .getJSONObject("story"));
        }

        return storyObjects;
    }

    private static List<String> getWireURLs() throws IOException, JSONException {
        List<String> urls = new ArrayList<String>();
        JSONObject json = JsonReader.readJsonFromUrl("https://wireapi.reuters.com/v3/feed/url/www.reuters.com/theWire");
        JSONArray wireItems = json.getJSONArray("wireitems");

        //System.out.println(wireItems);
        for (int i = 0; i < wireItems.length(); i++) {
            if(wireItems.getJSONObject(i).getString("wireitem_type").equals("story")){
                JSONArray templates = wireItems.getJSONObject(i).getJSONArray("templates");
                JSONObject story = templates.getJSONObject(0).getJSONObject("story");
                urls.add("https://wireapi.reuters.com/v3/feed/rcom/us/article/news:" + story.getString("usn"));
            }
        }
        return urls;
    }

    private static String getStoryBody(JSONObject story) throws JSONException {
        StringBuilder str = new StringBuilder();
        Document doc = Jsoup.parse(story.getString("body"));

        for(Element sentence : doc.select("p")){
            str.append(sentence.toString() + "\n");
        }

        return str.toString();
    }

    private static String getStoryHeadline(JSONObject story) throws JSONException{
        return story.getString("hed");
    }

    private static String getStoryShareURL(JSONObject story) throws JSONException{
        return story.getString("share_url");
    }

    private static String getStoryPublished(JSONObject story) throws JSONException{
        return story.getString("published_at");
    }
}
