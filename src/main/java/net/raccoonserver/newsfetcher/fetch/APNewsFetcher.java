package net.raccoonserver.newsfetcher.fetch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.raccoonserver.newsfetcher.utils.JsonReader;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class APNewsFetcher {
    public static String generateRSS() throws IOException, JSONException {
        StringBuilder str = new StringBuilder();

        str.append("<?xml version='1.0' encoding='UTF-8'?>\n");
        str.append("<rss version='2.0' xmlns:atom='http://www.w3.org/2005/Atom' xmlns:dc='http://purl.org/dc/elements/1.1/' >\n");
        str.append("<channel>\n");
        str.append("<title>Associated Press</title>\n");
        str.append("<language>en-us</language>\n");
        str.append("<link>https://www.apnews.com/apf-topnews</link>\n");
        str.append("<description>Top Headline's from the Associated Press.</description>\n");

        for(JSONObject story : getStoryObjects(getStoryURLs())){
            str.append("<item>\n");
            str.append("<title><![CDATA[" + getStoryHeadline(story) + "]]></title>\n");
            str.append("<link>" + getStoryLocalLinkUrl(story) + "</link>\n");

            str.append("<description><![CDATA[<p>\n");
            str.append(getStoryHTML(story));
            str.append("\n]]></description>\n");

            str.append("<pubDate>" + getStoryPublished(story) + "</pubDate>\n");
            str.append("</item>\n");
        }

        str.append("</channel></rss>");
        return str.toString();
    }

    private static List<JSONObject>getStoryObjects(List<String> storyURLs) throws IOException, JSONException {
        List<JSONObject> storyObjects = new ArrayList<JSONObject>();

        for(String storyURL : storyURLs){
            storyObjects.add(JsonReader.readJsonFromUrl(storyURL));
        }

        return storyObjects;
    }

    private static List<String> getStoryURLs() throws IOException, JSONException {
        List<String> urls = new ArrayList<String>();
        JSONObject json = JsonReader.readJsonFromUrl("https://afs-prod.appspot.com/api/v2/feed/tag?tags=apf-topnews");
        JSONArray test = json.getJSONArray("cards");
        for (int i = 0; i < test.length(); i++) {
            urls.add("https://storage.googleapis.com/afs-prod/contents/" + test.getJSONObject(i).getString("id"));
        }
        return urls;
    }

    private static String getStoryHTML(JSONObject story) throws JSONException{
        StringBuilder str = new StringBuilder();
        Document doc = Jsoup.parse(story.getString("storyHTML"));

        for(Element sentence : doc.select("p")){
            str.append(sentence.toString() + "\n");
        }

        return str.toString();
    }

    private static String getStoryHeadline(JSONObject story) throws JSONException{
        return story.getString("headline");
    }

    private static String getStoryLocalLinkUrl(JSONObject story) throws JSONException{
        return story.getString("localLinkUrl");
    }

    private static String getStoryPublished(JSONObject story) throws JSONException{
        return story.getString("published");
    }
}
