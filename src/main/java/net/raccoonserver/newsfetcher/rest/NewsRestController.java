package net.raccoonserver.newsfetcher.rest;

import net.raccoonserver.newsfetcher.fetch.APNewsFetcher;
import net.raccoonserver.newsfetcher.fetch.ReutersNewsFetcher;
import org.json.JSONException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;
import java.time.LocalDateTime;

@RestController
public class NewsRestController {
    @GetMapping("/")
    public String getLanding(){
        return "Hello World! The time is now " + LocalDateTime.now();
    }

    @GetMapping(value="/ap", produces={"application/xml"})
    public String getAPFeed() throws IOException, JSONException {
        return APNewsFetcher.generateRSS();
    }

    @GetMapping(value="/reuters", produces={"application/xml"})
    public String getReutersFeed() throws IOException, JSONException {
        return ReutersNewsFetcher.generateRSS();
    }

    @GetMapping("/teapot")
    public ResponseEntity getTeapot(){
        return new ResponseEntity<>("The requested entity body is short and stout. Tip me over and pour me out.", HttpStatus.I_AM_A_TEAPOT);
    }
}
