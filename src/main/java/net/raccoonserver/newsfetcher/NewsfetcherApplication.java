package net.raccoonserver.newsfetcher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NewsfetcherApplication {

	public static void main(String[] args) {
		SpringApplication.run(NewsfetcherApplication.class, args);
	}

}
